using MVCToDoList.Models;

namespace MVCToDoList.ViewModels
{
    public class TodoOverviewViewModel
    {
        public string Id { get; set; }

        public string EmployeeId { get; set; }

        public string Title { get; set; }

        public PriorityStatus Priority { get; set; }
    }
}