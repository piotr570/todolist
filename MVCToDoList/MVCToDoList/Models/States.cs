namespace MVCToDoList.Models
{
    public enum States
    {
        New,
        Active,
        Resolved,
        Closed
    }
}