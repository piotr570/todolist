namespace MVCToDoList.Models
{
    public enum PriorityStatus
    {
        Critical,
        Medium,
        Low
    }
}