using AutoMapper;
using MVCToDoList.Models;
using MVCToDoList.ViewModels;

namespace MVCToDoList.AutoMappers
{
    public class TodoProfile : Profile
    {
        public TodoProfile()
        {
            CreateMap<TodoDTO, TodoViewModel>();

            CreateMap<TodoDTO, TodoOverviewViewModel>();
        }
    }
}