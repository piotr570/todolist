using AutoMapper;
using MVCToDoList.Models;
using MVCToDoList.ViewModels;

namespace MVCToDoList.AutoMappers
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeDTO, EmployeeViewModel>();
        }
    }
}