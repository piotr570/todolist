namespace MVCToDoList.DB
{
    public class DbSettings : IDbSettings
    {
        public string EmployeesCollectionName { get; set; }
        public string TodosCollectionName { get; set; }
        
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}